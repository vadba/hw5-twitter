const request = async (method, url, body = null) => {
    const headers = {
        'Content-Type': 'application/json',
    };

    const res = await fetch(url, {
        method: method,
        headers: headers,
        ...(!!body && { body: JSON.stringify(body) }),
    });

    if (method !== 'DELETE') {
        return await res.json();
    } else {
        return await res;
    }
};
